# Cemboylu

Cemboylu-Be is a backend part of the stock automation Maven project using Java, Spring.

## Tech Stack

- Spring Boot
- Spring Boot Undertow
- Spring Actuator
- Spring Jpa
- Spring MVC
- Spring Security
- Postgres SQL
- Lombok
- JWT
- Liquibase
- Mockito
- Power Mockito

## Installation

``mvn clean install``
