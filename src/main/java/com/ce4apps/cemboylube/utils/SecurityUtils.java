package com.ce4apps.cemboylube.utils;

import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class SecurityUtils {

    public static Optional<String> getActiveUsername() {
        if (null == SecurityContextHolder.getContext() || null == SecurityContextHolder.getContext().getAuthentication()) {
            return Optional.empty();
        }
        return Optional.of(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public static void killActiveUser() {
        SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
    }

}
