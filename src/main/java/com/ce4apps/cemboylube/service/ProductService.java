package com.ce4apps.cemboylube.service;

import com.ce4apps.cemboylube.entitiy.dto.ProductDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {

    Page<ProductDto> getAllProducts(Pageable pageable);

    ProductDto getProduct(Long code);

    void saveProducts(List<ProductDto> products);

    void saveProduct(ProductDto productDto);

    void deleteProduct(Long code);
}
