package com.ce4apps.cemboylube.service;

import com.ce4apps.cemboylube.entitiy.dto.UserDto;
import org.springframework.http.ResponseEntity;

public interface UserService {

    ResponseEntity<String> createUser(UserDto userDto);

    UserDto validateAndFindUser(String username);

}
