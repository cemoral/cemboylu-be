package com.ce4apps.cemboylube.service;

public interface PasswordService {

    String encrypt(String strToEncrypt);

    String decrypt(String strToDecrypt);
}
