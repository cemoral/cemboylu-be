package com.ce4apps.cemboylube.service.impl;

import com.ce4apps.cemboylube.service.PasswordService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Service
public class PasswordServiceImpl implements PasswordService {

    @Value("${cemboylu.secret.key}")
    private String secretKey;
    @Value("${cemboylu.salt}")
    private String salt;
    @Value("${cemboylu.iv}")
    private String ivString;

    @Override
    public String encrypt(String strToEncrypt) {
        try {
            return Base64.getEncoder().encodeToString(initCipher().doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            log.error("Error while encrypting: " + e.toString());
        }
        return null;
    }

    @Override
    public String decrypt(String strToDecrypt) {
        try {
            return new String(Objects.requireNonNull(initCipher()).doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (Exception e) {
            log.error("Error while decrypting: " + e.toString());
        }
        return null;
    }

    private Cipher initCipher() throws Exception {
        byte[] iv = ivString.getBytes();
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), salt.getBytes(), 65536, 256);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKeySpec secretKeySpec = new SecretKeySpec(tmp.getEncoded(), "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        return cipher;
    }
}
