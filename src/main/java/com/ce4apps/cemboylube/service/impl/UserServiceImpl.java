package com.ce4apps.cemboylube.service.impl;

import com.ce4apps.cemboylube.entitiy.User;
import com.ce4apps.cemboylube.entitiy.dto.UserDto;
import com.ce4apps.cemboylube.exception.UserNotValidException;
import com.ce4apps.cemboylube.exception.UsernameExistException;
import com.ce4apps.cemboylube.repository.UserRepository;
import com.ce4apps.cemboylube.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

/**
 * Service class for managing users.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public ResponseEntity<String> createUser(UserDto userDto) {

        Optional<User> userOpt = userRepository.findByUsername(userDto.getUsername());
        try {
            if (!userOpt.isPresent()) {
                User user = new User();
                BeanUtils.copyProperties(userDto, user);
                user.setPassword(passwordEncoder.encode(userDto.getPassword()));
                userRepository.save(user);
                return ResponseEntity.ok(String.format("User created with username: %s.", user.getUsername()));
            } else {
                throw new UsernameExistException("Username is already exists.");
            }
        } catch (UsernameExistException e) {
            return ResponseEntity.badRequest().body(e.getLocalizedMessage());
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            return ResponseEntity.badRequest().body("Problem occurs when creating user.");
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username: " + username + " not found"));
    }

    @Override
    public UserDto validateAndFindUser(String username) {

        try {
            if (StringUtils.isEmpty(username)) {
                throw new UserNotValidException();
            }
            return userRepository.findByUsername(username).map(u -> {
                UserDto userDto = new UserDto();
                BeanUtils.copyProperties(u, userDto);
                return userDto;
            }).orElseThrow(() -> new UsernameNotFoundException("Username: " + username + " not found"));
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }
}