package com.ce4apps.cemboylube.service.impl;

import com.ce4apps.cemboylube.entitiy.Product;
import com.ce4apps.cemboylube.entitiy.User;
import com.ce4apps.cemboylube.entitiy.dto.ProductDto;
import com.ce4apps.cemboylube.exception.ProductNotFoundException;
import com.ce4apps.cemboylube.repository.ProductRepository;
import com.ce4apps.cemboylube.repository.UserRepository;
import com.ce4apps.cemboylube.service.ProductService;
import com.ce4apps.cemboylube.utils.CommonUtils;
import com.ce4apps.cemboylube.utils.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final UserRepository userRepository;

    @Override
    public Page<ProductDto> getAllProducts(Pageable pageable) {

        log.debug("Getting logged in user");
        final String username = SecurityUtils.getActiveUsername().orElseThrow(() -> new UsernameNotFoundException("There is no logged in user"));
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(String.format("%s not found in the database", username)));
        Page<Product> products = productRepository.findProductsByUserOrderByCreateDateDesc(user, pageable);
        log.debug("Getting products by user [{}]", user.getUsername());
        return new PageImpl<>(products.stream().map(ProductDto::new).collect(Collectors.toList()), products.getPageable(), products.getTotalElements());
    }

    @Override
    public ProductDto getProduct(Long code) {

        log.debug("Getting product with code: [{}]", code);
        Product product = productRepository.findByCode(code).orElseThrow(() -> new ProductNotFoundException(code + " not found"));
        return new ProductDto(product);
    }

    @Override
    public void saveProducts(List<ProductDto> products) {

        log.debug("Batch saving products");
        products.forEach(this::saveProduct);
    }

    @Override
    public void saveProduct(ProductDto productDto) {

        log.debug("Getting logged in user");
        final String username = SecurityUtils.getActiveUsername().orElseThrow(() -> new UsernameNotFoundException("There is no logged in user"));
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(String.format("%s not found in the database", username)));
        Product productDB = productRepository.findByCode(productDto.getCode()).orElseGet(Product::new);
        BeanUtils.copyProperties(productDto, productDB, CommonUtils.getNullPropertyNames(productDto));
        productDB.setUser(user);
        log.debug("Saving product with name [{}]", productDto.getName());
        productRepository.save(productDB);
    }

    @Override
    public void deleteProduct(Long code) {

        Optional<Product> product = productRepository.findByCode(code);
        log.debug("Deleting product with code [{}]", code);
        product.ifPresent(productRepository::delete);
    }
}
