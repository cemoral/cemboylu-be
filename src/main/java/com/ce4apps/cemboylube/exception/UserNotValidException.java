package com.ce4apps.cemboylube.exception;

public class UserNotValidException extends Exception {

    public UserNotValidException() {
        super();
    }

    public UserNotValidException(String message) {
        super(message);
    }
}
