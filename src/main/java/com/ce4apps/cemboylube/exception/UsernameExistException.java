package com.ce4apps.cemboylube.exception;

import javax.security.auth.login.LoginException;

public class UsernameExistException extends LoginException {

    public UsernameExistException() {
        super();
    }

    public UsernameExistException(String msg) {
        super(msg);
    }
}
