package com.ce4apps.cemboylube.exception;

public class ProductNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -433919358815117506L;

    public ProductNotFoundException(String msg) {
        super(msg);
    }
}
