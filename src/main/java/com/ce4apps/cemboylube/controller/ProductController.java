package com.ce4apps.cemboylube.controller;

import com.ce4apps.cemboylube.entitiy.dto.ProductDto;
import com.ce4apps.cemboylube.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api/product")
@RestController
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("/{pageNumber}/{pageSize}")
    public ResponseEntity<Page<ProductDto>> getAllProducts(@PathVariable int pageNumber, @PathVariable int pageSize) {

        Page<ProductDto> products = productService.getAllProducts(PageRequest.of(pageNumber, pageSize));
        return products.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(products);
    }

    @PostMapping
    public ResponseEntity<Void> addProduct(@Valid @RequestBody ProductDto productDto) {

        productService.saveProduct(productDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{code}")
    public ResponseEntity<Void> deleteProduct(@PathVariable(value = "code") String productCode) {

        productService.deleteProduct(Long.valueOf(productCode));
        return ResponseEntity.ok().build();
    }

    @PutMapping
    public ResponseEntity<ProductDto> updateProduct(@Valid @RequestBody ProductDto productDTO) {

        productService.saveProduct(productDTO);
        return ResponseEntity.ok().body(productDTO);
    }
}
