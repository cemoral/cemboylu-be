package com.ce4apps.cemboylube.config;

import com.ce4apps.cemboylube.service.PasswordService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@RequiredArgsConstructor
public class DataSourceConfig {

    @Value("${cemboylu.datasource.url}")
    private String url;
    @Value("${cemboylu.datasource.username}")
    private String username;
    @Value("${cemboylu.datasource.password}")
    private String password;
    private final PasswordService passwordService;

    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url(url);
        dataSourceBuilder.username(username);
        dataSourceBuilder.password(passwordService.decrypt(password));
        return dataSourceBuilder.build();
    }
}
