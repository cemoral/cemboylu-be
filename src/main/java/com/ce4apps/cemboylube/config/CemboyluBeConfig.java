package com.ce4apps.cemboylube.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CemboyluBeConfig {

    private static final String ENCODING_TYPE = "bcrypt";

    @Bean
    public PasswordEncoder passwordEncoder() {
        Map<String, PasswordEncoder> encoderMap = new HashMap<>();
        encoderMap.put(ENCODING_TYPE, new BCryptPasswordEncoder());
        return new DelegatingPasswordEncoder(ENCODING_TYPE, encoderMap);
    }

}
