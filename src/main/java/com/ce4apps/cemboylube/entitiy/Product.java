package com.ce4apps.cemboylube.entitiy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "product")
@SequenceGenerator(name = "seq", allocationSize = 100)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product extends BaseEntity {

    private static final long serialVersionUID = 791159884607764812L;

    @Column(unique = true)
    private Long code;

    private String name;

    private String description;

    @Column(name = "purchase_price")
    private double purchasePrice;

    @Column(name = "sale_price")
    private double salePrice;

    private int quantity;

    @ManyToOne
    @JoinColumn(name = "user_id_fk")
    @JsonIgnore
    private User user;

}
