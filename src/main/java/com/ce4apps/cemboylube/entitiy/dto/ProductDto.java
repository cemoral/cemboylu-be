package com.ce4apps.cemboylube.entitiy.dto;

import com.ce4apps.cemboylube.entitiy.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto implements Serializable {

    private static final long serialVersionUID = 5060754605142947696L;

    @NotNull(message = "Code cannot be null")
    private Long code;

    @NotBlank(message = "Name cannot be empty")
    private String name;

    private String description;

    private double purchasePrice;

    private double salePrice;

    private int quantity;

    private Date createDate;

    public ProductDto(Product product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.description = product.getDescription();
        this.purchasePrice = product.getPurchasePrice();
        this.salePrice = product.getSalePrice();
        this.quantity = product.getQuantity();
        this.createDate = product.getCreateDate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDto that = (ProductDto) o;
        return Double.compare(that.purchasePrice, purchasePrice) == 0 &&
                Double.compare(that.salePrice, salePrice) == 0 &&
                quantity == that.quantity &&
                Objects.equals(code, that.code) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(createDate, that.createDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, description, purchasePrice, salePrice, quantity, createDate);
    }
}
