package com.ce4apps.cemboylube;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CemboyluBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CemboyluBeApplication.class, args);
    }
}
