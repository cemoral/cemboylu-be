package com.ce4apps.cemboylube.repository;

import com.ce4apps.cemboylube.entitiy.Product;
import com.ce4apps.cemboylube.entitiy.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    Page<Product> findProductsByUserOrderByCreateDateDesc(User user, Pageable pageable);

    Optional<Product> findByCode(Long code);

}
